# Download Service

## Installation

Clone the repo.

Build the binaries:
```
go build -o bin/server src/cmd/server/main.go
go build -o bin/randomFileGenerator src/cmd/randomFileGenerator/main.go
```

Run the tests and view test coverage:
```
go test -coverprofile=c.out ./src
go tool cover -html=c.out
```

If desired, change configurations such as file paths, rate limits, etc. in `src/config.go`

## Run the service

Run `bin/randomFileGenerator` to generate random files in the `files/` folder, or put your designated files there.
Note: files need to follow this pattern: `{appName}|{os}|{version}.pkg`

The service runs on port 5000 by default.
Run the service:
```bin/server```

### Downloading files

Download a single file:

API `GET` `/download?file_identifiers=[{file_identifier}]`

Download multiple files:

API `GET` `/download?file_identifiers=[{file_identifier}]&file_identifiers=[{file_identifier}]&....`

### Browsing and searching for files

API: `GET` `/search?attr1=val1&attr2=val2&...&attrn=valn`

The set of attributes that users can base their search on is the following:

1. Application name - the name of the application requested for download.

    `appName`: `app_mobile`, `app_desktop`, `app_shift_planner`, `app_service`

2. Operating system - the OS required by the client, searching for the file

    `os`: `macos`, `linux`, `windows`

3. Version - the version of the application requested

    `version`: `1, 2, ..., n`

No attribute is required. If no attributes are specified, the results should be sorted by application name.
For matching application names, it should sort by version and for matching versions, sort by operating system.

Example request:
```json
    {
        "appName": "app_desktop",
        "os": "macos",
        "version": 3
    }
```

### Limiting and pagination

Use the parameters `limit` and `offset` (both optional).
