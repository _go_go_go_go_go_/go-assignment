package downloadService

import (
	"encoding/json"
	"os"
	"path/filepath"
	"strconv"
	"strings"
)

var (
	AppNames = []string{"app_desktop", "app_mobile", "app_service", "app_shift_planner"}
	OsNames  = []string{"linux", "macos", "windows"}
)

type Downloadable struct {
	Identifier string
	AppName    AppName
	Os         Os
	Version    Version
}

func (downloadable *Downloadable) GetPath() string {
	return filepath.Join(FilesPath + "/" + downloadable.Identifier + ".pkg")
}

func (downloadable *Downloadable) GetFileName() string {
	return downloadable.Identifier + ".pkg"
}

func (downloadable *Downloadable) GetFile() (*os.File, error) {
	file, err := os.Open(downloadable.GetPath())
	if err != nil {
		return nil, err
	}
	return file, nil
}

func (downloadable *Downloadable) MarshalJSON() ([]byte, error) {
	return json.Marshal(downloadable.Identifier)
}

func NewDownloadableFromIdentifier(identifier string) (*Downloadable, error) {
	splitIdentifier := strings.Split(identifier, "|")

	if len(splitIdentifier) != 3 {
		return nil, &InvalidIdentifier{}
	}

	appName, err := AppNameFromString(splitIdentifier[0])

	if err != nil {
		return nil, err
	}

	os, err := OsFromString(splitIdentifier[1])

	if err != nil {
		return nil, err
	}

	version, err := VersionFromString(splitIdentifier[2])

	if err != nil {
		return nil, err
	}

	return &Downloadable{
		identifier,
		appName,
		os,
		Version(version),
	}, nil

}

func NewDownloadableFromFileName(fileName string) (*Downloadable, error) {
	splitName := strings.Split(fileName, ".")
	if len(splitName) != 2 {
		return nil, &InvalidFile{}
	}
	if splitName[1] != "pkg" {
		return nil, &InvalidFile{}
	}

	return NewDownloadableFromIdentifier(splitName[0])

}

type InvalidIdentifier struct {
}

func (e *InvalidIdentifier) Error() string {
	return "Invalid identifier!"
}

type InvalidFile struct {
}

func (e *InvalidFile) Error() string {
	return "Invalid file!"
}

type Filter func(*Downloadable) bool

type Filterable interface {
	GenerateFilter() Filter
}

type FilterGeneratorFunc func(string) (Filter, error)

type AppName int

const (
	AppDesktop AppName = iota
	AppMobile
	AppService
	AppShiftPlanner
)

func (appName AppName) GenerateFilter() Filter {
	return func(downloadable *Downloadable) bool {
		return downloadable.AppName == appName
	}
}

func (appName AppName) String() string {
	return AppNames[appName]
}

func AppNameFromString(str string) (AppName, error) {
	for i := 0; i < len(AppNames); i++ {
		if AppNames[i] == str {
			return AppName(i), nil
		}
	}
	return 0, &InvalidAppName{}
}

func AppNameFilterFromString(str string) (Filter, error) {
	appName, err := AppNameFromString(str)
	if err != nil {
		return nil, err
	}
	return appName.GenerateFilter(), nil
}

type InvalidAppName struct {
}

func (e *InvalidAppName) Error() string {
	return "Invalid app name!"
}

type Os int

const (
	Linux Os = iota
	Macos
	Windows
)

func (os Os) GenerateFilter() Filter {
	return func(downloadable *Downloadable) bool {
		return downloadable.Os == os
	}
}

func (os Os) String() string {
	return OsNames[os]
}

func OsFromString(str string) (Os, error) {
	for i := 0; i < len(OsNames); i++ {
		if OsNames[i] == str {
			return Os(i), nil
		}
	}
	return 0, &InvalidOsName{}
}

func OsFilterFromString(str string) (Filter, error) {
	os, err := OsFromString(str)
	if err != nil {
		return nil, err
	}
	return os.GenerateFilter(), nil
}

type InvalidOsName struct {
}

func (e *InvalidOsName) Error() string {
	return "Invalid os name!"
}

type Version uint

func (version Version) GenerateFilter() Filter {
	return func(downloadable *Downloadable) bool {
		return downloadable.Version == version
	}
}

func (version Version) String() string {
	return strconv.Itoa(int(version))
}

func VersionFromString(str string) (Version, error) {
	result, err := strconv.Atoi(str)
	if err != nil {
		return 0, err
	}
	if result <= 0 {
		return 0, &InvalidVersion{}
	}
	return Version(result), nil
}

func VersionFilterFromString(str string) (Filter, error) {
	version, err := VersionFromString(str)
	if err != nil {
		return nil, err
	}
	return version.GenerateFilter(), nil
}

type InvalidVersion struct {
}

func (e *InvalidVersion) Error() string {
	return "Invalid version!"
}
