package main

import (
	downloadService "downloadService/src"
	"fmt"
	"math/rand"
	"os"
	"time"
)

func main() {
	rand.Seed(time.Now().UnixNano())

	_, err := os.Stat(downloadService.FilesPath)

	if err == nil {
		fmt.Println("Clearing files folder")
		err = os.RemoveAll(downloadService.FilesPath)
		if err != nil {
			panic(err)
		}
	}
	os.Mkdir(downloadService.RootPath+"/files", 0755)
	var appName downloadService.AppName
	var fileOs downloadService.Os
	var version downloadService.Version
	var appString string

	for i := 0; i < len(downloadService.AppNames); i++ {
		appName = downloadService.AppName(i)
		for j := 0; j < len(downloadService.OsNames); j++ {
			fileOs = downloadService.Os(j)
			for k := 1; k < 5; k++ {
				version = downloadService.Version(k)
				appString = appName.String() + "|" + fileOs.String() + "|" + version.String()
				fmt.Println("Creating file for: ", appName, fileOs, version)
				file, err := os.Create(downloadService.RootPath + "/files/" + appString + ".pkg")
				if err != nil {
					panic(err)
				}
				numberOfMegabytes := rand.Intn(30-5) + 5
				for i := 0; i < numberOfMegabytes; i++ {
					buffer := make([]byte, 1000000)
					rand.Read(buffer)
					_, err = file.Write(buffer)
					if err != nil {
						fmt.Println("Error writing bytes to %s!", appString)
						continue
					}
				}
				file.Close()
			}
		}
	}
}
