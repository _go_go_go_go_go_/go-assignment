package downloadService

import (
	"strconv"
	"testing"
)

var (
	service *DownloadService
)

func init() {
	service = &DownloadService{
		Downloadables: map[string]*Downloadable{},
		ClientRates:   map[string]int{},
	}
	var downloadable *Downloadable
	var fileName string
	for i := 0; i < len(CorrectAppNames); i++ {
		for j := 0; j < len(CorrectOsNames); j++ {
			for version := 1; version < 5; version++ {
				fileName = CorrectAppNames[i] + "|" + CorrectOsNames[j] + "|" + strconv.Itoa(version) + ".pkg"
				downloadable, _ = NewDownloadableFromFileName(fileName)
				service.Downloadables[downloadable.Identifier] = downloadable
			}
		}
	}
}

func TestSearch(t *testing.T) {
	var results []*Downloadable
	for i := 0; i < len(CorrectAppNames); i++ {
		appNameFilter, _ := AppNameFilterFromString(CorrectAppNames[i])
		appName, _ := AppNameFromString(CorrectAppNames[i])
		results = service.SearchDownloadables([]Filter{appNameFilter})
		for k := 0; k < len(results); k++ {
			if results[k].AppName != appName {
				t.Error(appName.String() + " should have matched!")
			}
		}
		for j := 0; j < len(CorrectOsNames); j++ {
			osFilter, _ := OsFilterFromString(CorrectOsNames[j])
			os, _ := OsFromString(CorrectOsNames[j])
			results = service.SearchDownloadables([]Filter{osFilter})
			for k := 0; k < len(results); k++ {
				if results[k].Os != os {
					t.Error(os.String() + " should have matched!")
				}
			}
			results = service.SearchDownloadables([]Filter{appNameFilter, osFilter})
			for k := 0; k < len(results); k++ {
				if results[k].Os != os || results[k].AppName != appName {
					t.Error(os.String() + " and " + appName.String() + " should have matched!")
				}
			}
			for version := 1; version < 10; version++ {
				versionFilter, _ := VersionFilterFromString(strconv.Itoa(version))
				version, _ := VersionFromString(strconv.Itoa(version))
				results = service.SearchDownloadables([]Filter{versionFilter})
				for k := 0; k < len(results); k++ {
					if results[k].Version != version {
						t.Error(version.String() + " should have matched!")
					}
				}
				results = service.SearchDownloadables([]Filter{appNameFilter, versionFilter})
				for k := 0; k < len(results); k++ {
					if results[k].Version != version || results[k].AppName != appName {
						t.Error(version.String() + " and " + appName.String() + " should have matched!")
					}
				}
				results = service.SearchDownloadables([]Filter{osFilter, versionFilter})
				for k := 0; k < len(results); k++ {
					if results[k].Version != version || results[k].Os != os {
						t.Error(version.String() + " and " + os.String() + " should have matched!")
					}
				}
				results = service.SearchDownloadables([]Filter{appNameFilter, osFilter, versionFilter})
				for k := 0; k < len(results); k++ {
					if results[k].AppName != appName || results[k].Version != version || results[k].Os != os {
						t.Error(appName.String() + " and " + version.String() + " and " + os.String() + " should have matched!")
					}
				}
			}
		}
	}
	results = service.SearchDownloadables([]Filter{})
	if len(results) != len(service.Downloadables) {
		t.Error("No filter should have returned all results!")
	}
}

func TestSort(t *testing.T) {
	results := service.SearchDownloadables([]Filter{})
	results = service.SortResults(results)
	for i := 0; i < len(results)-1; i++ {
		if results[i].AppName > results[i+1].AppName {
			t.Error("Sorting is wrong!")
			t.Error(results[i].AppName, ">", results[i+1].AppName)
			break
		}
		if results[i].AppName == results[i+1].AppName && results[i].Version > results[i+1].Version {
			t.Error("Sorting is wrong!")
			t.Error(results[i].Version, ">", results[i+1].Version)
			break
		}
		if results[i].AppName == results[i+1].AppName && results[i].Version == results[i+1].Version && results[i].Os > results[i+1].Os {
			t.Error("Sorting is wrong!")
			t.Error(results[i].Os, ">", results[i+1].Os)
			break
		}
	}
}

func TestPagination(t *testing.T) {

	originalResults := service.SearchDownloadables([]Filter{})

	var paginatedResults []*Downloadable

	for offset := -5; offset < len(originalResults)+5; offset++ {
		for limit := -5; limit < 50; limit++ {
			paginatedResults = service.PaginateResults(originalResults, offset, limit)
			if (limit <= 0 || offset >= len(originalResults)) && len(paginatedResults) > 0 {
				t.Error("There should have been no results!")
				return
			}
			if limit <= 0 || offset >= len(originalResults) {
				continue
			}
			if offset < 0 {
				offset = 0
			}
			if limit < 0 {
				limit = 0
			}
			if paginatedResults[0] != originalResults[offset] {
				t.Error("First result is wrong!")
				return
			}
			lastElementIndex := offset + limit
			if offset+limit > len(originalResults) {
				lastElementIndex = len(originalResults)
			}
			if paginatedResults[len(paginatedResults)-1] != originalResults[lastElementIndex-1] {
				t.Error("Last result is wrong!")
				return

			}

		}
	}

}

func TestRateLimits(t *testing.T) {
	ipAddresses := []string{"8.8.8.8", "4.4.4.4"}
	for resets := 0; resets < 3; resets++ {
		for i := 0; i < RateLimitMax+50; i++ {
			for j := 0; j < len(ipAddresses); j++ {
				err := service.CheckRateLimit(ipAddresses[j])
				if err != nil && i < RateLimitMax {
					t.Error("Rate limit should not have been reached!")
				}
				if err == nil && i >= RateLimitMax {
					t.Error("Rate limit should have been reached!")
				}
			}
		}
		service.ResetRateLimits()
	}
}
