package downloadService

import (
	"path/filepath"
	"strconv"
	"testing"
)

var (
	CorrectAppNames      = []string{"app_mobile", "app_desktop", "app_shift_planner", "app_service"}
	IncorrectAppNames    = []string{"APP_APP", "app_56456", "werwerwer_desktop"}
	CorrectOsNames       = []string{"macos", "linux", "windows"}
	IncorrectOsNames     = []string{"LinuxMint", "maxos", "windoxs"}
	IncorrectFileNames   = []string{"asdlkajsklads", "erferf.ertert", "ergeg|wefwef.fegr", "ergerg.pkg.pkg"}
	IncorrectIdentifiers = []string{"ergeg|wer", "sjgklerjg", "xcv|xcv|xcv|xcv", "|||||"}
)

func TestNewDownloadableFromFile(t *testing.T) {
	var downloadable *Downloadable
	var fileName string
	var err error

	// positives first

	for i := 0; i < len(CorrectAppNames); i++ {
		for j := 0; j < len(CorrectOsNames); j++ {
			for version := 1; version < 5; version++ {
				fileName = CorrectAppNames[i] + "|" + CorrectOsNames[j] + "|" + strconv.Itoa(version) + ".pkg"
				downloadable, err = NewDownloadableFromFileName(fileName)

				if err != nil {
					t.Error("For file name " + fileName + ": " + err.Error())
					continue
				}
				if downloadable.GetPath() != filepath.Join(FilesPath+"/"+fileName) {
					t.Error("File path should have been " + filepath.Join(FilesPath+"/"+fileName) + "!")
				}

				if downloadable.AppName.String() != CorrectAppNames[i] {
					t.Error("App name is wrong!")
				}

				if downloadable.Os.String() != CorrectOsNames[j] {
					t.Error("Os is wrong!")
				}

				if downloadable.Version.String() != strconv.Itoa(version) {
					t.Error("Version is wrong!")
				}

			}

			// now negatives

			for version := 0; version > -5; version-- {
				fileName = CorrectAppNames[i] + "|" + CorrectOsNames[j] + "|" + strconv.Itoa(version) + ".pkg"

				_, err = NewDownloadableFromFileName(fileName)

				if _, ok := err.(*InvalidVersion); !ok {
					t.Error("File " + fileName + " should have been wrong version!")
				}
			}
		}
		for j := 0; j < len(IncorrectOsNames); j++ {
			for version := -5; version < 5; version++ {
				fileName = CorrectAppNames[i] + "|" + IncorrectOsNames[j] + "|" + strconv.Itoa(version) + ".pkg"

				_, err = NewDownloadableFromFileName(fileName)

				if _, ok := err.(*InvalidOsName); !ok {
					t.Error("File " + fileName + " should have been wrong os!")
				}
			}
		}
	}
	for i := 0; i < len(IncorrectAppNames); i++ {
		for j := 0; j < len(CorrectOsNames); j++ {
			for version := -5; version < 5; version++ {
				fileName = IncorrectAppNames[i] + "|" + CorrectOsNames[j] + "|" + strconv.Itoa(version) + ".pkg"

				_, err = NewDownloadableFromFileName(fileName)

				if _, ok := err.(*InvalidAppName); !ok {
					t.Error("File " + fileName + " should have been wrong app name!")
				}
			}
		}
	}

	for i := 0; i < len(IncorrectFileNames); i++ {
		fileName = IncorrectFileNames[i]

		_, err = NewDownloadableFromFileName(fileName)

		if _, ok := err.(*InvalidFile); !ok {
			t.Error("File " + fileName + " should have been wrong filename!")
			t.Error(err)
		}
	}

	for i := 0; i < len(IncorrectIdentifiers); i++ {
		fileName = IncorrectIdentifiers[i]

		_, err = NewDownloadableFromIdentifier(fileName)

		if _, ok := err.(*InvalidIdentifier); !ok {
			t.Error("File " + fileName + " should have been wrong identifier!")
			t.Error(err)
		}
	}

}

func TestFilterGeneration(t *testing.T) {
	downloadable := &Downloadable{}
	for i := 0; i < len(CorrectAppNames); i++ {
		filter, err := AppNameFilterFromString(CorrectAppNames[i])
		if err != nil {
			t.Error(CorrectAppNames[i] + " filter should have been generated!")
			continue
		}
		if i > 0 {
			if filter(downloadable) {
				t.Error(CorrectAppNames[i] + " filter should have failed!")
				continue
			}
		}
		downloadable.AppName, _ = AppNameFromString(CorrectAppNames[i])
		if !filter(downloadable) {
			t.Error(CorrectAppNames[i] + " filter should have passed!")
			continue
		}
	}
	for i := 0; i < len(CorrectOsNames); i++ {
		filter, err := OsFilterFromString(CorrectOsNames[i])
		if err != nil {
			t.Error(CorrectOsNames[i] + " filter should have been generated!")
			continue
		}
		if i > 0 {
			if filter(downloadable) {
				t.Error(CorrectOsNames[i] + " filter should have failed!")
				continue
			}
		}
		downloadable.Os, _ = OsFromString(CorrectOsNames[i])
		if !filter(downloadable) {
			t.Error(CorrectOsNames[i] + " filter should have passed!")
			continue
		}
	}
	for i := 1; i < 10; i++ {
		filter, err := VersionFilterFromString(strconv.Itoa(i))
		if err != nil {
			t.Error(strconv.Itoa(i) + " filter should have been generated!")
			continue
		}
		if i > 0 {
			if filter(downloadable) {
				t.Error(strconv.Itoa(i) + " filter should have failed!")
				continue
			}
		}
		downloadable.Version, _ = VersionFromString(strconv.Itoa(i))
		if !filter(downloadable) {
			t.Error(strconv.Itoa(i) + " filter should have passed!")
			continue
		}
	}

	//negatives

	for i := 0; i < len(IncorrectAppNames); i++ {
		_, err := AppNameFilterFromString(IncorrectAppNames[i])
		if err == nil {
			t.Error(IncorrectAppNames[i] + " filter should have been wrong!")
			continue
		}
	}
	for i := 0; i < len(IncorrectOsNames); i++ {
		_, err := OsFilterFromString(IncorrectOsNames[i])
		if err == nil {
			t.Error(IncorrectOsNames[i] + " filter should have been wrong!")
			continue
		}
	}
	for i := -10; i <= 0; i++ {
		_, err := VersionFilterFromString(strconv.Itoa(i))
		if err == nil {
			t.Error(strconv.Itoa(i) + " filter should have been wrong!")
			continue
		}
	}

}
