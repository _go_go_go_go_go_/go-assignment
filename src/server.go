package downloadService

import (
	"encoding/json"
	"io"
	"net/http"
	"strconv"
)

type serviceHandler func(*DownloadService, http.ResponseWriter, *http.Request)

type InvalidRequest struct{}

func (e *InvalidRequest) Error() string {
	return "Request is not valid!"
}

type RateLimitReached struct{}

func (e *RateLimitReached) Error() string {
	return "Rate limit of max " + RateLimitDescriptor + " reached!"
}

type IdentifierNotFound struct{}

func (e *IdentifierNotFound) Error() string {
	return "Identifier not found!"
}

func httpMiddleware(service *DownloadService, handler serviceHandler) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		service.Log.Log(r.RemoteAddr + " accessed " + r.URL.String())

		err := service.CheckRateLimit(r.RemoteAddr)
		if err != nil {
			w.Write([]byte(err.Error()))
			service.Log.Error(err)
			return
		}
		handler(service, w, r)
	}
}

func downloadFileHandler(service *DownloadService, w http.ResponseWriter, r *http.Request) {
	query := r.URL.Query()

	fileIdentifiers, ok := query["file_identifiers"]
	if !ok || len(fileIdentifiers) == 0 {
		e := &InvalidRequest{}
		w.Write([]byte(e.Error()))
		service.Log.Error(e)
		return
	}

	downloadables, err := service.GetDownloadables(fileIdentifiers)

	if err != nil || len(downloadables) == 0 {
		e := &IdentifierNotFound{}
		w.Write([]byte(e.Error()))
		service.Log.Error(e)
		return
	}

	downloadFileName := ""
	var contentLength int64

	for i := 0; i < len(downloadables); i++ {
		file, err := downloadables[i].GetFile()
		if err != nil {
			w.Write([]byte("Error reading file!"))
			service.Log.Error(err)
			return
		}
		downloadFileName += downloadables[i].GetFileName()
		fileStat, _ := file.Stat()
		contentLength += fileStat.Size()
		file.Close()
	}
	w.Header().Set("Content-Disposition", "attachment; filename="+downloadFileName)
	w.Header().Set("Content-Type", "application/octet-stream")
	w.Header().Set("Content-Length", strconv.FormatInt(contentLength, 10))

	for i := 0; i < len(downloadables); i++ {
		file, _ := downloadables[i].GetFile()
		io.Copy(w, file)
		file.Close()
	}
}

func searchHandler(service *DownloadService, w http.ResponseWriter, r *http.Request) {

	query := r.URL.Query()

	filters, err := service.GenerateFiltersFromQuery(query)

	if err != nil {
		w.Write([]byte(err.Error()))
		service.Log.Error(err)
		return
	}

	results := service.SearchDownloadables(filters)

	if len(filters) == 0 {
		results = service.SortResults(results)
	}

	var offset, limit int

	offsetValue, ok := query["offset"]
	if ok {
		offset, err = strconv.Atoi(offsetValue[0])
		if err != nil {
			e := &InvalidRequest{}
			w.Write([]byte(e.Error()))
			service.Log.Error(e)
			return
		}
	}
	limitValue, ok := query["limit"]
	if ok {
		limit, err = strconv.Atoi(limitValue[0])
		if err != nil {
			e := &InvalidRequest{}
			w.Write([]byte(e.Error()))
			service.Log.Error(e)
			return
		}
	}

	if offset > 0 || limit > 0 {
		results = service.PaginateResults(results, offset, limit)
	}

	w.Header().Set("Content-Type", "application/json")

	json, _ := json.Marshal(results)

	w.Write(json)
}

func StartServer() {
	server := &http.Server{
		Addr: ServiceAddr,
	}

	service := NewDownloadService()

	service.Log.Log("Downloadables list up to date! Ready to serve ...")

	http.HandleFunc("/download", httpMiddleware(service, downloadFileHandler))
	http.HandleFunc("/search", httpMiddleware(service, searchHandler))

	go service.WatchRateLimits()

	server.ListenAndServe()
}
