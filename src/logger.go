package downloadService

import (
	"fmt"
	"log"
	"os"

	"github.com/fatih/color"
)

type Logger struct {
	DebugStdoutLogger, DebugFileLogger, ErrorStdoutLogger, ErrorFileLogger *log.Logger
	DebugColor, ErrorColor                                                 func(...interface{}) string
	Debug                                                                  bool
	LogFlags                                                               int
}

func (logger *Logger) CreateStdoutLoggers(debugColor, errorColor color.Attribute) {
	logger.DebugStdoutLogger = log.New(os.Stdout, "D ", logger.LogFlags)
	logger.ErrorStdoutLogger = log.New(os.Stdout, "E ", logger.LogFlags)
	logger.DebugColor = color.New(debugColor).SprintFunc()
	logger.ErrorColor = color.New(errorColor).SprintFunc()
}

func (logger *Logger) CreateFileLoggers(path string) {
	os.Mkdir(LogsPath, 0755)
	debugLogFile, err := os.Open(DebugLogPath)
	if err != nil {
		debugLogFile, err = os.Create(DebugLogPath)
		if err != nil {
			fmt.Println("COULD NOT CREATE DEBUG LOG FILE!!!")
		}
	}
	errorLogFile, err2 := os.Open(ErrorLogPath)
	if err2 != nil {
		errorLogFile, err2 = os.Create(ErrorLogPath)
		if err2 != nil {
			fmt.Println("COULD NOT CREATE ERROR LOG FILE!!!")
		}
	}
	logger.DebugFileLogger = log.New(debugLogFile, "D ", logger.LogFlags)
	logger.ErrorFileLogger = log.New(errorLogFile, "E ", logger.LogFlags)
}

func (logger *Logger) Log(v ...interface{}) {
	if logger.Debug {
		logger.DebugStdoutLogger.Output(2, logger.DebugColor(v...))
	}
	logger.DebugFileLogger.Output(2, fmt.Sprint(v...))
}

func (logger *Logger) Error(v ...interface{}) {
	if logger.Debug {
		logger.ErrorStdoutLogger.Output(2, logger.ErrorColor(v...))
	}
	logger.ErrorFileLogger.Output(2, fmt.Sprint(v...))
}
