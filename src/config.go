package downloadService

import (
	"path/filepath"
	"runtime"
	"time"
)

var (
	_, b, _, _ = runtime.Caller(0)

	ServiceAddr        = ":5000"
	RootPath           = filepath.Join(filepath.Dir(b), "../")
	FilesPath          = filepath.Join(RootPath, "files/")
	LogsPath           = filepath.Join(RootPath, "logs/")
	DebugLogPath       = filepath.Join(LogsPath, "debug.log")
	ErrorLogPath       = filepath.Join(LogsPath, "error.log")
	RateLimitTimeframe = time.Minute * 1
	// RateLimitTimeframe  = time.Second * 30
	RateLimitMax        = 30
	RateLimitDescriptor = "30 requests per 1 minute"
)
