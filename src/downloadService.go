package downloadService

import (
	"errors"
	"log"
	"net/url"
	"os"
	"sort"
	"time"

	"github.com/fatih/color"
)

type DownloadService struct {
	Downloadables map[string]*Downloadable
	Filterables   map[string]FilterGeneratorFunc
	Log           Logger
	ClientRates   map[string]int
}

func (service *DownloadService) getFileList() error {

	f, err := os.Open(FilesPath)

	if err != nil {
		return err
	}

	defer f.Close()

	files, err := f.Readdir(-1)

	if err != nil {
		return err
	}

	for _, file := range files {
		downloadable, err := NewDownloadableFromFileName(file.Name())
		if err != nil {
			return err
		}
		service.Downloadables[downloadable.Identifier] = downloadable

	}

	return nil
}

func (service *DownloadService) GetDownloadable(fileIdentifier string) (*Downloadable, error) {
	downloadable, ok := service.Downloadables[fileIdentifier]
	if !ok {
		return nil, errors.New("No such file found!")
	}
	return downloadable, nil
}

func (service *DownloadService) GetDownloadables(fileIdentifiers []string) ([]*Downloadable, error) {
	downloadables := []*Downloadable{}
	for i := 0; i < len(fileIdentifiers); i++ {
		downloadable, err := service.GetDownloadable(fileIdentifiers[i])
		if err != nil {
			return nil, err
		}
		downloadables = append(downloadables, downloadable)
	}
	return downloadables, nil
}

func (service *DownloadService) SearchDownloadables(filters []Filter) []*Downloadable {
	results := make([]*Downloadable, len(service.Downloadables))
	i := 0
	for _, downloadable := range service.Downloadables {
		results[i] = downloadable
		i++
	}
	if len(filters) == 0 {
		return results
	}

	filteredResults := make([]*Downloadable, 0, len(service.Downloadables))

	for _, result := range results {
		keep := true
		for _, filter := range filters {
			if !filter(result) {
				keep = false
				break
			}
		}

		if keep {
			filteredResults = append(filteredResults, result)
		}
	}

	return filteredResults
}

func (service *DownloadService) SortResults(results []*Downloadable) []*Downloadable {
	sort.Slice(results, func(i, j int) bool {
		if results[i].AppName == results[j].AppName {
			if results[i].Version == results[j].Version {
				return results[i].Os < results[j].Os
			}
			return results[i].Version < results[j].Version
		}
		return results[i].AppName < results[j].AppName
	})
	return results
}

func (service *DownloadService) PaginateResults(results []*Downloadable, offset int, limit int) []*Downloadable {
	if offset < 0 {
		offset = 0
	}
	if limit < 0 {
		limit = 0
	}
	if offset > len(results) {
		return nil
	}
	if offset+limit > len(results) {
		return results[offset:len(results)]
	}
	return results[offset : offset+limit]
}

func (service *DownloadService) GenerateFiltersFromQuery(query url.Values) ([]Filter, error) {
	filters := []Filter{}

	for filterable, filterFunc := range service.Filterables {
		filterValue, ok := query[filterable]
		if ok {
			filter, err := filterFunc(filterValue[0])
			if err != nil {
				return nil, err
			}
			filters = append(filters, filter)
		}
	}

	return filters, nil
}

func (service *DownloadService) CheckRateLimit(ipAddress string) error {
	clientRate, ok := service.ClientRates[ipAddress]
	if !ok {
		clientRate = 0
	}
	if clientRate >= RateLimitMax {
		e := &RateLimitReached{}
		return e
	}
	clientRate += 1
	service.ClientRates[ipAddress] = clientRate
	return nil
}

func (service *DownloadService) ResetRateLimits() {
	service.ClientRates = map[string]int{}
}
func (service *DownloadService) WatchRateLimits() {
	for {
		service.ResetRateLimits()
		time.Sleep(RateLimitTimeframe)
	}
}

func NewDownloadService() *DownloadService {
	service := &DownloadService{
		Downloadables: map[string]*Downloadable{},
		Filterables:   map[string]FilterGeneratorFunc{"appName": AppNameFilterFromString, "os": OsFilterFromString, "version": VersionFilterFromString},
		ClientRates:   map[string]int{},
	}
	logger := Logger{
		Debug:    true,
		LogFlags: log.Ldate | log.Ltime | log.Lmicroseconds | log.Llongfile,
	}
	logger.CreateFileLoggers(RootPath)
	logger.CreateStdoutLoggers(color.FgBlue, color.FgHiRed)
	logger.Log("Initialized service!")
	service.Log = logger

	err := service.getFileList()

	if err != nil {
		panic(err)
	}
	return service
}
